package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Light {

    Texture texture;
    Texture headTexture;

    Vector2 origin;
    Vector2 dimension;
    Vector2 scale;
    Vector2 position;

    float flickEffectTime;

    public Light(Texture texture, Texture headTexture, Vector2 position) {
        this.texture = texture;
        this.headTexture = headTexture;

        dimension = new Vector2(texture.getWidth(), texture.getHeight());
        origin = new Vector2(dimension.x / 2f, dimension.y / 2f);
        scale = new Vector2(1, 1);
        this.position = position;
    }

    void update(float delta) {
        applyFlickEffect(delta);
    }

    private void applyFlickEffect(float delta) {
        flickEffectTime += delta;
        while (flickEffectTime >= 0.035f) {
            float newScale = MathUtils.clamp(scale.x + MathUtils.random(-0.03f, 0.05f), 0.8f, 1.0f);
            scale.set(newScale, newScale);
            flickEffectTime = -0.035f;
        }
    }

    void drawLight(SpriteBatch batch) {
        batch.draw(
                texture, position.x - origin.x, position.y - origin.y,
                origin.x, origin.y,
                dimension.x, dimension.y,
                scale.x, scale.y,
                0,
                0, 0,
                (int) dimension.x, (int) dimension.y,
                false, false);
    }

    void drawHead(SpriteBatch batch) {
        batch.draw(headTexture, position.x - headTexture.getWidth() / 2f, position.y - headTexture.getHeight() / 2f);
    }
}
