package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class MyGdxGame extends ApplicationAdapter {

    private static final int SCENE_WIDTH = 1024;
    private static final int SCENE_HEIGHT = 576;

    SpriteBatch batch;
    Texture map;
    Texture lightTexture;
    Texture lightHead;
    Texture white;

    OrthographicCamera camera;
    Viewport viewport;

    FrameBuffer lightBuffer;
    Sprite lightBufferSprite = new Sprite();

    Vector2 touchPos = new Vector2();
    Vector2 lightPos = new Vector2();

    Light light;

    @Override
    public void create() {
        batch = new SpriteBatch();
        map = new Texture("map.jpg");
        lightTexture = new Texture("light.png");
        lightHead = new Texture("lightHead.png");
        white = new Texture("white.png");
        light = new Light(lightTexture, lightHead, lightPos);

        camera = new OrthographicCamera();
        camera.position.set(new Vector3(960, 500, 1));

        viewport = new ExtendViewport(SCENE_WIDTH, SCENE_HEIGHT, camera);

        lightBuffer = buildFrameBuffer(lightBufferSprite, SCENE_WIDTH, SCENE_HEIGHT);
    }

    private FrameBuffer buildFrameBuffer(Sprite sprite, int width, int height) {
        FrameBuffer frameBuffer = new FrameBuffer(Pixmap.Format.RGB888, width, height, false);
        sprite.setTexture(frameBuffer.getColorBufferTexture());
        sprite.setSize(width, height);
        sprite.setRegion(0, 0, width, height);
        sprite.flip(false, true);
        return frameBuffer;
    }

    @Override
    public void render() {
        float delta = Gdx.graphics.getDeltaTime();

        if (Gdx.input.isKeyPressed(Input.Keys.UP))
            camera.position.y += 100 * delta;
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
            camera.position.y -= 100 * delta;
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
            camera.position.x -= 100 * delta;
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            camera.position.x += 100 * delta;
        if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
            camera.zoom = MathUtils.clamp(camera.zoom - 1 * delta, 0.1f, 4f);
            lightBufferSprite.setScale(camera.zoom);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.X)) {
            camera.zoom = MathUtils.clamp(camera.zoom + 1 * delta, 0.1f, 4f);
            lightBufferSprite.setScale(camera.zoom);
        }

        light.update(delta);

        touchPos.set(Gdx.input.getX(), Gdx.input.getY());
        viewport.unproject(touchPos);
        light.position.set(touchPos);

        lightBufferSprite.setPosition(
                camera.position.x - camera.viewportWidth * camera.zoom / 2f,
                camera.position.y - camera.viewportHeight * camera.zoom / 2f);


        createLightBuffer();

        viewport.apply();

        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.setColor(1f, 1f, 1f, 1f);
        batch.draw(map, 0, 0);
        light.drawHead(batch);
        batch.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
        lightBufferSprite.draw(batch);
        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        batch.end();
    }

    private void createLightBuffer() {
        lightBuffer.begin();

        Gdx.gl.glClearColor(0.025f, 0.05f, 0.1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        batch.setColor(0.99f, 0.84f, 0.46f, 1f);
        light.drawLight(batch);
        batch.end();

        FrameBuffer.unbind();
    }

    @Override
    public void dispose() {
        batch.dispose();
        map.dispose();
        white.dispose();
        lightTexture.dispose();
        lightHead.dispose();
        lightBuffer.dispose();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        lightBuffer.dispose();
        lightBuffer = buildFrameBuffer(lightBufferSprite,
                (int) viewport.getWorldWidth(),
                (int) viewport.getWorldHeight());
    }
}
